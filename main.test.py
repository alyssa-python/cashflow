from dateutil import rrule
from datetime import datetime

PAY_DAY = rrule.rrule(
    dtstart=datetime(2020, 3, 13),      # First payday
    freq=rrule.WEEKLY,                  # Occurs
    interval=2,                         # biweekly
    byweekday=rrule.FR,                 # On Friday
)

# -------------------------------#

from datetime import datetime

PAYDAY_TEST_CASES = [
    ((datetime(2020, 1, 1), datetime(2020, 3, 1)),
     []),
    ((datetime(2020, 3, 1), datetime(2020, 4, 11)),
     [datetime(2020, 3, 13),
      datetime(2020, 3, 27),
      datetime(2020, 4, 10)]),
]

def test_payday():
    for (between_args, expected) in PAYDAY_TEST_CASES:
        assert PAY_DAY.between(*between_args) == expected

if __name__ == "__main__":
    test_payday()
    print('Success!')