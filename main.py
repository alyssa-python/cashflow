from datetime import * 
from dateutil.relativedelta import *
from dateutil.rrule import *
from dateutil.parser import *
import numpy as np
import pandas as pd
import os
import requests

import pprint
import sys

sys.displayhook = pprint.pprint

BASE_URL = f"https://api.airtable.com/v0/{os.getenv('AIRTABLE_BASE_CASHFLOW')}"

TODAY = date.today()
START = date(2020, 3, 13)
END = date(2021, 12, 31)
# END = date(2025, 8, 25)

# every other friday starting on 3/13/2020
PAYDAYS = list(rrule(WEEKLY, interval=2,
    byweekday=FR,
    dtstart=START,
    until=END
))

FUTURE_PAYDAYS = [date for date in PAYDAYS if datetime.date(date) >= TODAY]

# amounts = [250] * len(PAYDAYS)

paydays = np.array(
    # PAYDAYS
    # PAYDAYS[4:], # get index of first date after today
    FUTURE_PAYDAYS
    # amounts
) # , np.full((1, len(PAYDAYS)), 250)

# print(datetime.date(PAYDAYS[0]) > TODAY)
# print(PAYDAYS.size)
df = pd.DataFrame(paydays)

print(df)
# print(np.full((1, len(PAYDAYS)), 250)[0])
# print(amounts)
# df.to_csv('paydays.csv')



# batch in groups of 10
# create and attach to RRULE


# Make an API call and store the response.
url = f"{BASE_URL}/RRULE"
headers = { 
    "Authorization": f"Bearer {os.getenv('AIRTABLE_API_KEY')}",
    # "Content-Type": "application/json"
}
r = requests.get(url, headers=headers)
# print(f"Status code: {r.status_code}")
# Store API response in a variable.
response_dict = r.json()

# Process results.
print(response_dict['records'])
print(f"Got {len(response_dict['records'])} records(s)\n")

# Save to DB
data = {
    "fields": {
        "Name": "TEST PAYDAY",
        "Notes": "Created with Python",
        "Status": "Todo",
        "starting on": START.strftime('%Y-%m-%d'),
        "frequency": "weekly",
        "interval": 2,
        "byWeekDay": [
            "Friday"
        ],
        # "Account": [],
        "Estimate": 5800,
    }
}

p = requests.post(url, headers=headers, json=data)
post_dict = p.json()

print(post_dict)

# Delete from DB

# print(f"DELETE {url}/{post_dict['id']}")
d = requests.delete(f"{url}/{post_dict['id']}", headers=headers)
print(d.json())